# coding=UTF-8

from brokers import BaseBroker
from youtrack.connection import Connection
import re

class YouTrackBroker(BaseBroker):
    # Search a given text message for a string of the form "#AB-123 by username: state"
    # and return all matches. The part "by username" is optional.
    def find_issues(self, message):
        pattern = re.compile('#(\w+-\d+)\s*(.+):\s*(\w+)')
        return pattern.findall(message)

    def handle(self, payload):
        url = payload['service']['url']
        username = payload['service']['username']
        password = payload['service']['password']

        del payload['broker']
        del payload['service']

        youtrack = Connection(url, username, password)

        for commit in payload['commits']:
            issues = self.find_issues(commit['message'])
            for issue_found in issues:
                issue = youtrack.getIssue(issue_found[0])
                if issue_found[2].lower() == 'fixed':
                    youtrack.executeCommand(issue.id, 'Fixed')
